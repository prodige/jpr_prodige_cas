# Changelog

All notable changes to [jpr_prodige_cas](https://gitlab.adullact.net/prodige/jpr_prodige_cas) project will be documented in this file.

## [4.4.4](https://gitlab.adullact.net/prodige/jpr_prodige_cas/compare/4.4.3...4.4.4) - 2023-09-28

## [4.4.3](https://gitlab.adullact.net/prodige/jpr_prodige_cas/compare/4.4.2...4.4.3) - 2023-09-28

## [4.4.2](https://gitlab.adullact.net/prodige/jpr_prodige_cas/compare/4.4.1...4.4.2) - 2023-09-26

## [4.4.1](https://gitlab.adullact.net/prodige/jpr_prodige_cas/compare/4.4.0-rc9...4.4.1) - 2023-08-25

## [4.4.0-rc9](https://gitlab.adullact.net/prodige/jpr_prodige_cas/compare/4.4.0-rc8...4.4.0-rc9) - 2023-01-26

