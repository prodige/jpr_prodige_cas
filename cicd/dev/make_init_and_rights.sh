#!/bin/bash

# openldap
mkdir -p ./data/openldap/config
chmod 755 ./data/openldap/config 
mkdir -p ./data/openldap/database
chmod 755 ./data/openldap/database
mkdir -p ./data/openldap/add_init_ldif
mkdir -p ./var/cas/m2
chmod 777 ./var/cas/m2
find ./data/openldap/add_init_ldif -type d -exec chmod 0755 {} \;
find ./data/openldap/add_init_ldif -type f -exec chmod 0644 {} \;

mkdir -p ./var/openldap/backup
mkdir -p ./var/openldap/backup/data
mkdir -p ./var/openldap/backup/config
find ./var/openldap/backup -type d -exec chmod 0755 {} \;
find ./var/openldap/backup -type f -exec chmod 0644 {} \;
