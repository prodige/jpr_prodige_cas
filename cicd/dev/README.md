# README
<!-- TOC -->

- [README](#readme)
  - [Overview](#overview)
  - [Start developpement](#start-developpement)
    - [Cas Java compile the war file](#cas-java-compile-the-war-file)
    - [Deploy war](#deploy-war)
      - [Debug](#debug)
- [Follow symfony logs](#follow-symfony-logs)
  - [Stop developpement](#stop-developpement)
  - [Debug ldap](#debug-ldap)
    - [LDAP data](#ldap-data)
    - [Chagne LDAP Password](#chagne-ldap-password)
  - [Import backup](#import-backup)
  - [General Docker commands](#general-docker-commands)
  - [Advanced Docker commands](#advanced-docker-commands)
    - [Clean commands](#clean-commands)
    - [Monitoring commands](#monitoring-commands)

<!-- /TOC -->

## Overview

This file give commands to start development with docker.

Require:
 - Docker installed.
 - Docker well configured, cf. **/etc/docker/deamon.json**
 - User in docker group to avoid sudo/su right

## Start developpement

Init rights
```bash
sudo ./make_init_and_rights.sh
```

### Cas Java compile the war file

Make these commands in the root directory of this project:

```bash
docker run -u root -w "/usr/local/tomcat/build" --volume "${PWD}:/usr/local/tomcat/build" --volume "${PWD}/cicd/dev/var/cas/m2:/root/.m2" tomcat:9-jdk11-openjdk ./gradlew clean build
```

If you need custom theme :
```bash
docker run -u root -w "/usr/local/tomcat/build" --volume "${PWD}:/usr/local/tomcat/build" --volume "${PWD}/cicd/dev/var/cas/m2:/root/.m2" tomcat:9-jdk11-openjdk ./gradlew listTemplateViews
```

### Deploy war

Make these commands in this directory the ```./cicd/dev```

Run container
```bash
# Build dev image
docker-compose build

# Run containers
docker-compose up
```

The url web are: http://localhost:8080/

#### Debug

In an other terminal, connect as www-data to the dev container
```bash
# Connect to container
docker exec -it jpr_prodige_cas_dev bash
```

# Follow symfony logs
```bash
tail -f /etc/cas/log/*.log
```



## Stop developpement

Make these commands in this directory ```cd ./cicd/dev```.

```bash
# Shutdown containers
docker-compose down

# Remove dev image
docker rmi jpr_prodige_cas_dev_web
```

## Debug ldap


### LDAP data



(Option) erase data
```bash
sudo rm -rf ./data/openldap/config/* ./data/openldap/database/*
```

Debug:
```bash
# Test adminconfig connection
#     ldapsearch  -x -D cn=admin,cn=config -w ldapconfigpassword -LLL -b cn=config
# Test admin connection
#     ldapsearch  -LLL -H ldap://localhost -x -D "cn=admin,dc=alkante,dc=domprodige" -w ldappassword -b dc=alkante,dc=domprodige
# Add ldiff
#     ldapadd -H ldap://localhost -x -D "cn=admin,cn=config" -w ldapconfigpassword -f /ldif_test/01_schema_alkante.ldif
#     #ldapadd -H ldap://localhost -x -D "cn=admin,cn=config" -w ldapconfigpassword -f /ldif_test/03_ppolicy.ldif
#     ldapmodify -H ldap://localhost -x -D "cn=admin,cn=config" -w ldapconfigpassword -f /ldif_test/04_ppolicy-module.ldif
#     ldapadd -H ldap://localhost -x -D "cn=admin,cn=config" -w ldapconfigpassword -f /ldif_test/05_ppolicy-conf.ldif
#     ldapmodify -H ldap://localhost -x -D "cn=admin,cn=config" -w ldapconfigpassword -f /ldif_test/06_SHA512.ldif
#     ldapadd -H ldap://localhost -x -D "cn=admin,dc=alkante,dc=domprodige" -w ldappassword -f /ldif_test/102_p4.ldif
#     ldapadd -H ldap://localhost -x -D "cn=admin,dc=alkante,dc=domprodige" -w ldappassword -f /ldif_test/103_ppolicy-default.ldif
```

### Chagne LDAP Password
generate password SHA512 crypted :

```bash
python3 -c "import crypt, base64;
password='';
password=str('{CRYPT}%s' % crypt.crypt(
        'clipassword',
        crypt.mksalt(
            crypt.METHOD_SHA512
        )
    ));
b_password = password.encode('utf-8');
en_b_password = base64.b64encode(b_password);
en_password = en_b_password.decode('utf-8');
print('password: ',password);
print('b_password: ',b_password);
print('pasen_b_passwordsword: ',en_b_password);
print('en_password: ',en_password)"
```

Result:
adminclipassword

You can change password in **openldap/db/02_p4.ldif** for:
- phpcli  in  ```dn: uid=admincli,ou=Users,dc=alkante,dc=domprodige```
  - clipassword : e0NSWVBUfSQ2JFB5UVNEMmNxOTJ0azVlQjgkd1YxTFNwLjVrL2NJMVkuVWlEQ1JZN3ZEcFdDRE5IOGRaR3ovQ0k4a2tEbHhCTU52c3VOVkJvVFhMRXBsck9leHAuQVBUd2VxSngySWNWUjhyY3VTOTE=
- admin@prodige-opensource.org  in ```dn: uid=admin@prodige-opensource.org ,ou=Users,dc=alkante,dc=domprodige```
  - prodige01 : e0NSWVBUfSQ2JGNpbjI2aEk5TjUyY3JjYXYkZ3JoNEF2bUxHYVA0eWYySWhhNjBpVUZUQm0xbU9NT2FLeEluTThuNTFIeGliTVYya2RvaUwyaDZLajlPNFQzRC5lSUdXUDlqa2xoSUJiODVOMVcvWS4=
- Internet in ```dn: uid=Internet,ou=Users,dc=alkante,dc=domprodige```
  - ??????

Test connection:
```bash
ldapsearch  -LLL -H ldap://localhost -x -D "uid=admincli,ou=Users,dc=alkante,dc=domprodige" -w clipassword -b uid=admincli,ou=Users,dc=alkante,dc=domprodige
ldapsearch  -LLL -H ldap://localhost -x -D "uid=admin@prodige-opensource.org ,ou=Users,dc=alkante,dc=domprodige" -w prodige01 -b uid=admin@prodige-opensource.org ,ou=Users,dc=alkante,dc=domprodige
```

## Import backup

Create backup directory with ```./make_init_and_rights.sh```.

Now copy backup in **./var/openldap/backup/** and add number prefix in name to order le loading process:
Examples:
- 0_LDAP_cn=config.ldif
- 1_LDAP_dc=alkante,dc=domprodige.ldif

Re run ```./make_init_and_rights.sh``` to fix rigths



Change this in docker-compose:
```yml
    volumes:
        - ./data/openldap/database:/var/lib/ldap    # LDAP database files
        - ./data/openldap/config:/etc/ldap/slapd.d  # LDAP config files
        - ./var/openldap/restore:/restore:ro
...
    entrypoint: sleep 30000
    #command: ["--copy-service", "--logleve","trace"]
```

Run only ldap
```bash
docker-compose up jpr_prodige_cas_dev_ldap
```

Connect to container and run import
```bash
docker exec -it jpr_prodige_cas_dev_ldap bash
```
```bash
[ -d /var/lib/ldap ] || mkdir -p /var/lib/ldap
[ -d /etc/ldap/slapd.d ] || mkdir -p /etc/ldap/slapd.d
chown -R openldap:openldap /var/lib/ldap
chown -R openldap:openldap /etc/ldap
slapadd -F /etc/ldap/slapd.d -b cn=config -l /restore/0_LDAP_cn\=config.ldif
slapadd -F /etc/ldap/slapd.d -b dc=alkante,dc=domprodige -l /restore/LDAP_dc\=alkante\,dc\=domprodige.ldif
exit
```

Now shudown container

```bash
docker-compose down
```

Reverte docker-compose change
```yml
    volumes:
        - ./data/openldap/database:/var/lib/ldap    # LDAP database files
        - ./data/openldap/config:/etc/ldap/slapd.d  # LDAP config files
        #- ./var/openldap/restore:/restore:ro
...
    #entrypoint: sleep 30000
    command: ["--copy-service", "--loglevel","trace"]
```

Now you can run ldap with new data imported
```bash
docker-compose up
```


## General Docker commands

| Description | Commandes |
|- |- |
| Display images            | ```docker images``` |
| Remove image              | ```docker rmi myimages``` |
|- |- |
| Display all running containers | ```docker ps``` |
| Display all containers    | ```docker ps -a``` |
| Remove container          | ```docker rm mycontainer``` |
| Display info container    | ```docker inpect mycontainer``` |
| Display container ouput   | ```docker logs mycontainer``` |
| Stop container            | ```docker stop mycontainer``` |
| Kill container            | ```docker kill mycontainer``` |
|- |- |
| Connection as root | ```docker exec --user 0 -w / -it jpr_prodige_cas_dev_web bash``` |
| Connection as www-data | ```docker exec --user www-data -w /var/www/html -it jpr_prodige_cas_dev_web bash``` |
| Connection as postgres | ```docker exec --user postgres -it jpr_prodige_cas_dev_db bash``` |
| Run composer install | ```docker exec --user www-data -w /var/www/html -it jpr_prodige_cas_dev_web composer install ``` |

## Advanced Docker commands


### Clean commands
```bash
# Stop all container
docker stop $(docker ps -a -q)

# Remove all container
docker rm $(docker ps -a -q)

# Delete all image with name or tag as "<none>"
docker rmi `docker images| egrep "<none>" |awk '{print $3}'`
```

### Monitoring commands

```bash
# Watch all container
watch -n 1 "docker ps -a"

# Watch all images
watch -n 1 "docker images"

# Watch all resources container
docker stats
```
