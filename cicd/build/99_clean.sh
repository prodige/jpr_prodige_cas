#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`
. $SCRIPTDIR/env.sh

echo "  Start 99_clean.sh"

# Reset directory rigths
echo -n "LOCAL_USER_ID=$(id -u)\nLOCAL_GROUP_ID=$(id -g)" > $SCRIPTDIR/docker-build/docker-compose.env;
docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml up -d
docker exec --user root -w /usr/local/tomcat/build ${DOCKER_NAME}_build_web /bin/bash -c " \
  if [ -d "./build" ] ; then \
     chown -R "$(id -u)":"$(id -g)" ./build; \
  fi;"

docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml down

# Clean container gerenated
list="${DOCKER_NAME}_web ${DOCKER_NAME}_ldap ${DOCKER_NAME}_build_web ${DOCKER_NAME}_build_ldap"
for var in "${DOCKER_NAME}_web ${DOCKER_NAME}_ldap ${DOCKER_NAME}_build_web ${DOCKER_NAME}_build_ldap"
do
  if [ "$(docker ps -aq -f name=${var})" ]; then
    echo "  Clean container ${var}"
    docker rm -f ${var}
  fi
done

# Clean iamge gerenated
list=" ${DOCKER_NAME}_build_web ${DOCKER_NAME}_build_ldap"
for var in $list
do
  if [ "$(docker images -q -f reference=${var})" ]; then
    echo "  Clean images ${var}"
    docker rmi ${var}
  fi
done

echo "  End 99_clean.sh"
