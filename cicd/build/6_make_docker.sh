#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`
ROOTDIR="$SCRIPTDIR/../.."
COMMIT=`git rev-parse --short=8 HEAD`
DATE="`date '+%Y%m%dT%H%M'`"
. $SCRIPTDIR/env.sh

# Build docker
cd $SCRIPTDIR/docker-prod/ldap
docker build -t ${DOCKER_NAME}_ldap:${COMMIT} .
cd $ROOTDIR
docker build -t ${DOCKER_NAME}_web:${COMMIT} -f $ROOTDIR/cicd/build/docker-prod/web/Dockerfile .

# Tag to latest
docker tag  ${DOCKER_NAME}_ldap:${COMMIT} ${DOCKER_NAME}_ldap
docker tag  ${DOCKER_NAME}_web:${COMMIT} ${DOCKER_NAME}_web
